# 项目概述
本程序用于读取Access、SQL Server、MySQL等数据库，生成数据字典和实体类。

![软件界面截图](https://images.gitee.com/uploads/images/2019/0621/163604_2ce91601_27856.png "20190621163543.png")
* 若不是默认端口，SQL Server在服务器文本框中填写如"127.0.0.1,1433"，其它数据库在服务器文本框中填写如"127.0.0.1:3306"。

# 数据库配置SqlList.config
* SqlList.config格式如下：

```
<?xml version="1.0" encoding="utf-8" ?>
<SqlList>
  <Sql ConnType="MySql">
    <GetTablesSql>
      select TABLE_NAME as Name,TABLE_TYPE as Type,TABLE_COMMENT as Comment from information_schema.tables where TABLE_SCHEMA='{1}' {0} and TABLE_TYPE='BASE TABLE' order by Name
    </GetTablesSql>
    <GetColumnsSql>
      SELECT COLUMN_COMMENT as Comment, case when COLUMN_KEY='PRI' then 1 else 0 end as IsPrimaryKey, CHARACTER_MAXIMUM_LENGTH as MaxLength, DATA_TYPE as DataType, case when IS_NULLABLE='YES' then 1 else 0 end as IsNullable, COLUMN_DEFAULT as DefaultValue, COLUMN_NAME as Name,case when EXTRA='auto_increment' then 1 else 0 end as IsAutoIncrease
      FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='{1}' and TABLE_NAME =  '{0}'
    </GetColumnsSql>
  </Sql>
</SqlList>
```
* 该配置文件用于配置每种数据库获取表和字段元信息的SQL语句，SqlList中每种数据库使用一个Sql节点来配置，ConnType代表数据库类型（OleDb、MsSql2000、MsSql2005、MsSql2008、MySql、Oracle）；
* GetTablesSql为该数据库获取表的SQL，参数{0}代表其它筛选条件占位符，参数{1}代表数据库名称占位符，返回数据集的字段别名分别为Name（表名）、Type（类型）、Comment（表备注）；
* GetColumnsSql为该数据库获取字段的SQL，参数{0}代表表名称占位符，参数{1}代表数据库名称占位符，返回数据集的字段别名分别为Name（字段名）、IsPrimaryKey（是否主键）、IsNullable（是否可空）、IsAutoIncrease（是否自增）、DataType（数据类型）、MaxLength（最大长度）、DefaultValue（默认值）、Comment（字段备注）。

# 数据类型映射DataTypeList

* DataTypeList目录中为各数据库的数据类型和C#数据类型的映射关系；
* 文件名以数据库类型编码命名（OleDb、MsSql2000、MsSql2005、MsSql2008、MySql、Oracle）；
* Code为数据库中数据类型的编码，Name为数据库中数据类型的名称，CsType为C#中对应的数据类型名，DbType为C#中ADO.NET对应的数据类型名。

# 导出文件模板Template
* DbDicTpl.html为数据字段导出文件模板，使用NVelocity模板引擎语法，输出内容可以使用HTML、Markdown等；
* ModelTpl.cstpl为实体类导出文件模板，使用NVelocity模板引擎语法，输出内容可以使用C#类格式。
* 通过Table标签获取表信息；
* 通过this.GetColumn(Table,i)获取字段信息，i为索引；
* 通过this.GetCsType(this.GetColumn(Table,i))获取字段对应的C#数据类型。